

const hello = "https://72c9-185-184-111-34.eu.ngrok.io/";

export type Frame = {
    firstBall: number
    secondBall?: number
}

export type Game = {
    totalScore: number
    frames: Frame[]
}

export class BowlingGame {
    frames: Frame[] = [];
    totalScore: number = 0;

    private frameIsFinished(frame: Frame): boolean {
        return frame.firstBall === 10 || (!!frame.firstBall && !!frame.secondBall)
    }
    
    private updateTotalScore(): void {
        let newScore = 0;
        for(let i = 0; i < this.frames.length; i++) {
            
            const cFrame = this.frames[i];
            const isSpare = (cFrame.firstBall < 10) && (cFrame.secondBall) && ((cFrame.firstBall + cFrame.secondBall) === 10);
            const isStrike = cFrame.firstBall === 10;
            
            if(isStrike) {
                
            } else if (isSpare) {
                
            } else {
                newScore +
            }
            
            
            
        }
        
    }
    
    private updateTotalScore_(): void {
        let accumulator = 0;
        for(let i = 0; i < this.frames.length; i++) {
            accumulator += this.frames[i].firstBall + (this.frames[i].secondBall || 0);
            // strike and there is a next ball
            if(this.frames[i].firstBall === 10 && i + 1 < this.frames.length) {
                accumulator += this.frames[i + 1].firstBall;
                // frame + 1 = strike AND we have a frame + 2
                if (this.frames[i + 1].firstBall === 10) {
                    if (i + 2 < this.frames.length) {
                        accumulator += this.frames[i + 2].firstBall;
                    }
                } else {
                    accumulator += this.frames[i + 1].secondBall || 0;
                }
            }
            // spare and there is a next ball
            if((this.frames[i].firstBall + (this.frames[i].secondBall || 0) === 10) && i + 1 < this.frames.length) {
                accumulator += this.frames[i + 1].firstBall;
            }
        }
        this.totalScore = accumulator;
    }

    public addThrow(value: number): void {
        const current = this.frames.pop();
        if(current) {
            // There is an incomplete frame
            if (!this.frameIsFinished(current)) {
                current.secondBall = value;
                this.frames.push(current);
                this.totalScore += value;
            } else {
                this.frames.push(current);
                this.frames.push({firstBall: value});
                this.totalScore += value;
            }
        } else {
            this.frames.push({firstBall: value});
            this.totalScore += value;
        }
        this.updateTotalScore();
    }
}


