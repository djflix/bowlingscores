import { BowlingGame } from ".";

// describe("formatScores", () => {
//   it("outputs the skeleton of a score string", () => {
//     expect(formatScores([])).toEqual("score([]) -> 0");
//   });

//   it("calculates the final score", () => {
//     expect(formatScores([1, 2, 3])).toContain(" -> 6");
//   });
  
//   it.skip("formats a game of zeroes", () => {
//     expect(formatScores([0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ])).toEqual("score([0,0, 0,0, 0,0, 0,0, 0,0])")
//   });
// });


describe("The Game Progress", () => {
    
    it("A Game that just started has no 'frames' and totalScore 0", () => {
        const game = new BowlingGame();
        expect(game.totalScore).toEqual(0);
        expect(game.frames.length).toEqual(0); 
    });
    
    it("A throw of 5 => the game has 1 frame and the score is 5", () => {
        const game = new BowlingGame();
        game.addThrow(5);
        expect(game.totalScore).toEqual(5);
        expect(game.frames.length).toEqual(1); 
    });
    
    it("2 throws (5&4) result in a sum of totalScore 9", () => {
        const game = new BowlingGame();
        game.addThrow(5);
        game.addThrow(4);
        expect(game.totalScore).toEqual(9);
    });
    
    it("2 throws (5&4) result in 1 frame", () => {
        const game = new BowlingGame();
        game.addThrow(5);
        game.addThrow(4);
        expect(game.frames.length).toEqual(1);
    });
    
    it("3 throws (5&4&1) result in 2 frame", () => {
        const game = new BowlingGame();
        game.addThrow(5);
        game.addThrow(4);
        game.addThrow(1);
        expect(game.frames.length).toEqual(2);
    });
    
    it("3 throws (5&4&1) result in score 10", () => {
        const game = new BowlingGame();
        game.addThrow(5);
        game.addThrow(4);
        game.addThrow(1);
        expect(game.totalScore).toEqual(10);
    });
    
    it("2 throws (10&0) result in 2 frame", () => {
        const game = new BowlingGame();
        game.addThrow(10);
        game.addThrow(0);
        expect(game.frames.length).toEqual(2);
    });

    it("4 throws result in 2 frames", () => {
        const game = new BowlingGame();
        game.addThrow(1);
        game.addThrow(1);
        game.addThrow(1);
        game.addThrow(1);
        expect(game.frames.length).toEqual(2);
    })
    
    it("5 throws result in 3 frames", () => {
        const game = new BowlingGame();
        game.addThrow(1);
        game.addThrow(1);
        game.addThrow(1);
        game.addThrow(1);
        game.addThrow(1);
        expect(game.frames.length).toEqual(3);
    })
    
    it("4 throws (5&5&5&0) results in a score of 20", () => {
        const game = new BowlingGame();
        game.addThrow(5);
        game.addThrow(5);
        game.addThrow(5);
        game.addThrow(0);
        expect(game.totalScore).toEqual(20);
    });

    it("4 throws (5&5&5&5) results in a score of 25", () => {
        const game = new BowlingGame();
        game.addThrow(5);
        game.addThrow(5);
        game.addThrow(5);
        game.addThrow(5);
        expect(game.totalScore).toEqual(25);
    });

    it("3 throws (10&5&0) results in a score of 20", () => {
        const game = new BowlingGame();
        game.addThrow(10);
        game.addThrow(5);
        game.addThrow(0);
        expect(game.totalScore).toEqual(20);
    });

    it("3 throws (10&5&5) results in a score of 20", () => {
        const game = new BowlingGame();
        game.addThrow(10);
        game.addThrow(5);
        game.addThrow(5);
        expect(game.totalScore).toEqual(20);
    });

    it("3 throws (10&5&5&5) results in a score of 35", () => {
        const game = new BowlingGame();
        game.addThrow(10);
        game.addThrow(5);
        game.addThrow(5);
        game.addThrow(5);
        expect(game.totalScore).toEqual(35);
    });
})